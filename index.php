<?php
    require_once("Animal.php");
    require_once("Frog.php");
    require_once("Ape.php");
    
    $sheep = new Animal("shaun");
    echo "Nama: $sheep->name <br>"; // "shaun"
    echo "Jumlah kaki: $sheep->legs <br>"; // 2
    echo "Darah dingin? $sheep->cold_blooded <br> <br>"; // false

    $kodok = new Frog("buduk");
    echo "Name: $kodok->name <br>";
    echo "Jumlah kaki: $kodok->legs <br>";
    echo "Darah dingin? $kodok->cold_blooded <br>";
    $kodok->jump() ; // "hop hop"

    $sungokong = new Ape("kera sakti");
    echo "Name: $sungokong->name <br>";
    echo "Jumlah kaki: $sungokong->legs <br>";
    echo "Darah dingin? $sungokong->cold_blooded <br>";
    $sungokong->yell() // "Auooo"
?>